"""
Serve API using Flask.
"""

import os                               # import files safely for any os
import sys
import json                             # for parsing javascript object notation
from flask import Flask, jsonify, request  # for serving web app
from flask_cors import CORS             # stop CORS errors
from passlib.hash import pbkdf2_sha256  # encrypted passwords
from datetime import datetime, timedelta
import python_jwt as jwt, jwcrypto.jwk as jwk

import db_commands as db

q = db.Query(os.path.join(sys.path[0], 'auth.db'))

app = Flask(__name__)
CORS(app)

JWT_security_key = jwk.JWK.generate(kty='RSA', size=2048)




@app.route('/borrow/<item_key>/<token>')
def borrow(item_key, token):
    """Borrow item by key, verifying JWT for permission to borrow."""
    
    # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
        
        
        

    except:
        return jsonify({"status":'Invalid session token'})
    
    now = datetime.now().strftime("%Y-%m-%d.%H:%M.")
    borrowRecord = (item_key+now, email, item_key, 'requested',\
                        now, 'unfinished')
    return jsonify(q.save_row_to_table('transactions', borrowRecord))

@app.route('/search/<field>/<value>')
def search(field, value):
    """Search for title."""
    
    allowed = ['author', 'isbn', 'title', 'location']
    if field not in allowed:
        return jsonify({'data':'Sorry, that doesn\'t work'})
    
    if field == 'title':
        book_data = []
        results = q.search('books', 'title', value)
        for book in results:
            book_json ={'key':book[0], 'title':book[1], 'format':book[2], \
                'first_published':book[3], 'this_published':book[4], 'pages':book[5]}
            book_data.append(book_json)

    return jsonify(book_data)

@app.route('/transactions/<token>', methods = ['GET'])
def transactions(token):
    """If admin, return transaction data for requested and borrowed books."""
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
    except:
        return jsonify({'status':401,'message':'JSON web token could not be verified.'})
    
    
        # then confirm admin role
    is_admin = q.search('admins', 'email', email)
    
    if len(is_admin) > 0:
        if is_admin[0][2] == 'admin':
            role = 'admin'
    else:
        role = 'user'
    
    if role == 'admin':
        # get transaction data
        requested = []
        borrowed = []
        
        records = q.search('transactions', 'fin_time', 'unfinished')
        for record in records:
            item_id = record[2]
            item_data = q.search('books', 'key', item_id)
            try:
                book_title = item_data[0][1]
            except IndexError:
                book_title = 'Could not be found'
            # ~ value_data = q.search('value', 'item_id', item_id)
            # ~ value = value_data[3]
            # for the meantime
            value = 5
            status = record[3]
            person_id = record[1]
            person_data = q.search('accounts', 'key', person_id)
            if len(person_data) == 1:
                person_data = person_data[0]
            else:
                person_data = (0,1,2,3,4,'not found',6,7,8)
            ## person_cols ~ 0'key', 1'permission', 2'phone', 3'email', 
            ## 4'postcode', 5'first_name', 6'last_name', 7'date_of_birth'
            first_name = person_data[5]
            last_name = person_data[6]
            phone = person_data[2]
            record_data = {
                            'transaction': record[0],
                            'item_id': item_id,
                            'person': person_data,
                            'phoneNumber': phone,
                            'title': book_title,
                            'value': value,
                            }
            if status == 'requested':
                requested.append(record_data)
            elif status == 'borrowed':
                borrowed.append(record_data)
        
        requested_number = len(requested)
        borrowed_number = len(borrowed)

        
        return jsonify({
            'status':200, 
            'message':'Request to see transaction data was successful.', 
            'requested':requested,
            'requested_number': requested_number,
            'borrowed_number': borrowed_number,
            'borrowed':borrowed,
            })
    else:
        return jsonify({'status': 401, 'message':'Account does not have administrator privileges.'})

   

@app.route('/login', methods = ['POST'])
def login():
    """Sign in user and return JSON Web Token to authenticate."""

    email = request.json['usr'].lower()
    pwd = request.json['pwd']
    usr_auth = q.search('auth','key', email)
    if usr_auth:
        # is user already signed up?
        pwd_hash = usr_auth[0][1]
    else:
        return jsonify({"status":"not found"}) 
    
    if pbkdf2_sha256.verify(pwd, pwd_hash):
        # if pwd verified, get account data and return with JSON web token
        payload = { 'email': email }
        token = jwt.generate_jwt(payload, JWT_security_key, 'PS256', timedelta(minutes=15))
        
        role = 'user'
        # role = 'user' unless 'admin'
        is_admin = q.search('admins', 'email', email)
        if len(is_admin) > 0:
            if is_admin[0][2] == 'admin':
                role = 'admin'
        
        response = jsonify({'jwt':token, 'status':'signed in', 'role':role})
        return response
    else:
        return jsonify({"status":"incorrect password"})

@app.route('/signup', methods = ['POST'])
def signup():
    """Sign up new user."""
    email = request.json['usr'].lower()
    pwd_hash = pbkdf2_sha256.hash(request.json['pwd'])
    row = ('email', 'pwd_hash', 'permission_time')
    row = (email, pwd_hash, datetime.now().strftime("%Y-%m-%d.%H:%M."))
    
    return jsonify(q.save_row_to_table('auth', row))

@app.route('/complete', methods = ['POST'])
def complete_profile():
    """Complete user profile."""
    
    token = request.json['token']
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
        
        # search accounts table for email
        if len(q.search('accounts', 'key', email, precise=True)) == 0:
            # ie. there is no entry for this email
            now = datetime.now().strftime("%Y-%m-%d.%H:%M.")
            row = (email,\
                    now,\
                    request.json['mobilePhone'],\
                    email,\
                    request.json['postcode'],\
                    request.json['firstName'],\
                    request.json['lastName'],\
                    request.json['dateOfBirth'])
            
        
        
            return jsonify(q.save_row_to_table('accounts', row))
        else:
            return jsonify({'status':'error','detail':'profile already complete'})
    except:
        return jsonify({"status":'error','detail':'Invalid session token'})
        

@app.route('/profile/<token>', methods = ['GET'])
def profile_data(token):
    """Get profile data."""
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
        
        # search accounts table for email
        
        # ~ return jsonify(len(q.search('accounts', 'email', email)))
        if len(q.search('accounts', 'key', email, precise=True)) == 0:
            return jsonify({'status':'no record'})
        else:
            return jsonify({'status':'record exists'})
        
    except:
        return jsonify({"status":'invalid session token'})
        
@app.route('/borrowed/<token>', methods = ['GET'])
def show_borrowed(token):
    """Get details of borrowed books for verified user."""
    
        # first verify the JWT
    try:
        header, claims = jwt.verify_jwt(token, JWT_security_key, ['PS256'])
        email = claims['email']
        
        

    except:
        return jsonify({"status":'invalid session token'})
   
    transactions = q.search('transactions', 'account', email)
    borrowed = []
    requested = []
    for action in transactions:
        item_number = action[2]
        status = action[3]
        item_data = q.search('books', 'key', action[2], precise=True)[0]
        title = item_data[1]
        json_dict = {'item':item_number,'status':status,'title':title}
        if action[5] == "unfinished":
            if status == 'borrowed':
                borrowed.append(json_dict)
            if status == 'requested':
                requested.append(json_dict)

    return jsonify({'status':'success','borrowed':borrowed,'requested':requested})

if __name__ == "__main__":
    
    app.run()

// Made with faith, hope and love, by PI Prescott. 2020.


var JWT = ""
var role = "user"

const buffer = "<br><br><br>"

const apiURL = "http://localhost:5000"
//~ const apiURL = "https://wordandspirit.pythonanywhere.com"

const inputSearch = document.getElementById("input-search");
inputSearch.addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            search()
        }
    }
);

function aa(){
    return
}

function search() {
    showMessage('Let me see what we can find for you...')
    phrase = document.getElementById('input-search').value
    getFetch(apiURL + '/search/title/'+phrase, returnSearch)
}

function confirmBorrow(data){
    if (data['status'] == 'successful'){showMessage('Book has been requested.')}
    else if (data['status'] == 'error'){showMessage("Hmm, that one doesn't seem to be available.")}
}

const comments = ["Ooh, that's a good one!",
            "That is one of my favourites.",
            "An excellent choice!",
            "You're sure to enjoy that one."
            ]

function borrow(key){
    let i = Math.floor(Math.random() * comments.length)
    showMessage(comments[i])
    if (role = 'admin'){showMessage('You cannot borrow books with your Admin account.')}
    else if (JWT!=""){getFetch(apiURL + '/borrow/'+key+'/'+JWT, confirmBorrow)}
    else {showMessage('You must be signed in to borrow items.'); show('footer')}
}

function returnSearch(json){
    term = document.getElementById('input-search').value

    showMessage(json.length+ ' results for "'+ term + '". ')
    searchResults = ""
    searchResults += buffer
    for (i = 0; i<json.length; i++){
        searchResults += showSearchResult(json[i])
    }
    searchResults += buffer
    searchResults += buffer
    document.getElementById('root').innerHTML = searchResults
}

function showSearchResult(json_element){
    
    html =  asParagraph(json_element["title"], 'return-search') 
            + asParagraph("<a onclick='borrow("+ json_element["key"] +")'>[Request to Borrow]</a>", 'borrow-button')
            + "<hr>"

    return html
}

function loginForm(){
    hide('footer')
    hide('header')
    formHTML = inputField('email', 'usr', 'name@email.com', 'input-text')
                + inputField('password', 'login-pwd', 'password', 'input-text')
                + "<br>" + newButton('loginFormButton', 'submitLogInForm()', 'ENTER')
                + "<br>"+ "<a onClick='signUpForm()'>or SIGN UP</a>"
    document.getElementById('root').innerHTML = asDiv('loginForm', 'sign-up-form show-margin', asParagraph(buffer + formHTML + buffer))
    const inputPwd = document.getElementById("login-pwd");
    inputPwd.addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            submitLogInForm()
            }
        }
    );
    document.getElementById('usr').focus()
}

function signUpForm(){
    hide('footer')
    hide('header')

    formHTML = inputField('email', 'usr', 'name@email.com', 'input-text')
                + inputField('password', 'pwd', 'password', 'input-text')
                + inputField('password', 'pwd2', 'confirm password', 'input-text')

                + "<br>" + newButton('signUpFormButton', 'submitSignUpForm()', 'REGISTER')
                + "<br>"+ "<a onClick='loginForm()'>or LOG IN</a>"
                

    
    document.getElementById('root').innerHTML = asDiv('signUpForm', 'sign-up-form show-margin', asParagraph(buffer + formHTML + buffer))

    const inputConfirmPwd = document.getElementById("pwd2");
    inputConfirmPwd.addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            submitSignUpForm()
            }
        }
    );
    document.getElementById('usr').focus()

    
}


function submitSignUpForm(){
    usr = document.getElementById('usr').value
    pwd = document.getElementById('pwd').value
    pwd2 = document.getElementById('pwd2').value
    
    if (pwd2!=pwd){
        showMessage('Check your password is typed correctly.')
        return}
 
    if (pwd.length < 6){
        showMessage("Passwords need at least 6 characters.")
        return
    }
 
    if (validate_email(usr)==true && pwd.length > 5){
        let pwd = document.getElementById('pwd').value
        let sign = {"usr":usr, "pwd":pwd}
        postFetch(apiURL + '/signup', sign, signUpResponse)
    }
    else {showMessage("Invalid email.")}  
    
}

function submitLogInForm(){
    showMessage('Super secure data algorithm time...')
    let usr = document.getElementById('usr').value
    let pwd = document.getElementById('login-pwd').value
    let sign = {"usr":usr, "pwd":pwd}
    postFetch(apiURL + '/login', sign, loggedInResponse)
}

function signUpResponse(json){
    if (json["status"] == "successful"){
        pwd = document.getElementById('pwd').value
        showMessage("Success! Now press Enter to log in.")
        loginForm()
        document.getElementById('usr').value = usr
        document.getElementById('login-pwd').value = pwd
        document.getElementById("loginFormButton").focus()
        }
    else {showMessage("That email address is already registered.")}
}

function signedUp(){
    loginForm()
    document.getElementById('usr').value = usr
    document.getElementById('login-pwd').value = pwd
    }


function loggedInResponse(data){
    
        
    if (data["status"] == "signed in"){
        showMessage('Login successful.')
        JWT= data["jwt"]
        role = data['role']
        if (data['role'] == 'admin'){
            accountButton = newButton('showAccountButton', 'showAdmin()', 'ADMIN ACCOUNT') 
            }
        else{accountButton = newButton('showAccountButton', 'showAccount()', 'SHOW ACCOUNT')}
        document.getElementById('footer').innerHTML =   accountButton 
                                                        + 'OR'
                                                        + newButton('signOutButton', 'signOut()', 'SIGN OUT')
        document.getElementById('root').innerHTML = ""
        show('header')
        show('footer')
        document.getElementById('input-search').value = ""
        if (data['role'] == 'admin'){showAdmin()}
        else{showAccount()}
        
        
        }
    else if (data["status"] == "not found"){showMessage('That email is not registered.')}
    else if (data["status"] == "incorrect password"){showMessage('Hmm... that was the wrong password.')}
    else {
        showMessage("Well that's strange...")}
    
}

function weAre(){
    mission = 'Our Mission: <br>'
                + 'Save souls, grow saints,<br>'
                + 'serve suffering humanity.'
    extraMessage = 'Join us! We always need volunteers.'
    document.getElementById('root').innerHTML = '<a onClick=showMessage(extraMessage)>'
                                                + asParagraph(mission, 'big') + '</a>'
    
    showHide('header'); showMessage('...the hour is coming,<br>'
    + 'and is now here...<br>'
    /*+ 'when the true worshipers<br>'
    + 'will worship the Father<br>'
    + 'in spirit and truth...<br>'*/
    + 'John 4:23.')    

}

function showAccount(){
    html = ""
    html += buffer
    html += asDiv('profile-div', 'account-div', '...profile data...') + '<hr>'
            + asDiv('borrowed-div', 'account-div', '...borrowed books...')
            + asDiv('list-div', 'account-div', '') + '<hr>'
            //~ + asDiv('catalog-div', 'account-div', '...catalog your own...') 
            + buffer + buffer
    document.getElementById('root').innerHTML = asParagraph(html, 'show-account')

    getProfile()
    showBorrowed()
    //~ catalog()
}

function showAdmin(){
    showMessage('Admin account')
    
    html = ""
    html += buffer
    html += asDiv('process-admin', 'account-div', '...finding Requested Books...') //requested
            + asDiv('list-div', 'account-div', '') + '<hr>' // add more
            + asDiv('newBooks-admin', 'account-div', 'Add new books to the database.') + '<hr>' // add more
            + buffer + buffer

    document.getElementById('root').innerHTML = asParagraph(html, 'show-account')
    // requested books
    
    adminLiveData()
    
    
    // add more books
    adminMore()
    
}

function adminLiveData(){
    console.log('need to get transaction data about requested and borrowed')
    
    getFetch(apiURL+'/transactions/'+JWT, showLiveData)
    
}

var requested = []
var borrowed = []

function showLiveData(data){
    console.log(data)
    requested = data['requested']
    borrowed = data['borrowed']
    
    bookCount = "There are <a onClick=list(requested)>" 
                + requested.length
                + "</a> requested books awaiting collection,<br>"
                + "and <a onClick=list(borrowed)>"
                + borrowed.length
                + "</a> borrowed books awaiting return." 
    document.getElementById('process-admin').innerHTML = asParagraph(bookCount, 'show-account')
}

function showAdminBooks(data){
    html = ""
    for (i=0; i<data.length; i++){
        html += asParagraph(data['title'] + ' ~ ' + data['person'], 'show-account')
    }
    document.getElementById('list-div').innerHTML = html
    
}

function adminMore(){
    console.log('load form to add more')
}

function getProfile(){

    getFetch(apiURL+'/profile/'+JWT, showProfile)
    
}

function showProfile(json){
    if (json['status'] == 'no record'){
        html = asParagraph('To borrow books, you must <a onClick="profileForm()">complete your profile</a>.')
        }
    else if (json['status'] == 'record exists'){
        html = asParagraph('Registration is complete!')
        }
    else if (json['status'] == 'invalid session token'){
        signOut()
    }
    else {console.log('unknown error')}
    document.getElementById('profile-div').innerHTML = html
}

function profileForm(){
    hide('header')
    hide('footer')
    hide('message')
    formHTML = inputField('text', 'firstName', 'First Name', 'input-text')
                + inputField('text', 'lastName', 'Last Name', 'input-text')
                + inputField('number', 'mobilePhone', 'UK Mobile Phone Number', 'input-text')
                + inputField('text', 'postcode', 'UK Postcode', 'input-text')
                + asParagraph('Essential: I consent to have my profile data stored. ' + inputField('checkbox', 'consent'))
                + asParagraph('Optional: Please send me newletters and things. ' + inputField('checkbox', 'newsletters'))
                + newButton('profileFormButton', 'submitProfileForm()', 'COMPLETE PROFILE')
    document.getElementById('root').innerHTML = asDiv('profileForm', 'sign-up-form show-margin', asParagraph(buffer + formHTML + buffer + buffer))
    document.getElementById('firstName').focus()
}

function submitProfileForm(){
    
    // in good faith
    let allValid = true;
    console.log('allValid at start:'+allValid)
    
    firstName = document.getElementById('firstName').value
    lastName = document.getElementById('lastName').value
    mobilePhone = document.getElementById('mobilePhone').value
    postcode = document.getElementById('postcode').value
    consent = document.getElementById('consent').checked
    newsletters = document.getElementById('newsletters').checked
    
    profileData = {'firstName':firstName,
                    'lastName':lastName,
                    'mobilePhone':mobilePhone,
                    'postcode':postcode,
                    'dateOfBirth':'deprecated',
                    'consent':consent,
                    'newsletters':newsletters,
                    'token': JWT}
                    
    // first validate everything
    if (firstName.length == 0){
        showMessage('Please enter your first name.')
        allValid = false;
        console.log('allValid at firstname:'+allValid)
        return
    }
    if (lastName.length == 0){
        showMessage('Please enter your last name.')
        allValid = false;
        console.log('allValid at lastname:'+allValid)
        return
    }
    if (mobilePhone.length==0){
        showMessage('Please enter your mobile number.')
        allValid = false;
        console.log('allValid at phone:'+allValid)
        return
    }
    if (postcode.length == 0){
        showMessage('Please enter your postcode.')
        allValid = false;
        console.log('allValid at postcode:'+allValid)
    } else {
        // do more sophisticated postcode verification

    }
    
    if (consent == false){
        showMessage('We need your consent to store your data.')
        allValid = false;
        console.log('allValid at consent:'+allValid)
        return
    }
    

    
    if (allValid == true){
           postFetch( apiURL+'/complete', profileData, responseProfileForm)
    }
    


}



function responseProfileForm(json){
    if (json['status'] == 'successful'){showAccount()}
}

function catalog(){
    console.log('TODO: invitation to catalog your own books')
    
}

function showBorrowed(){
    getFetch(apiURL+'/borrowed/'+JWT, engageBorrowed)
    
}

function engageBorrowed(data){
    borrowed = data.borrowed
    requested = data.requested
    count = 'You have <a onClick="list(borrowed)">borrowed '
            + data['borrowed'].length + ' books </a>,<br>'
            + 'and <a onClick="list(requested)">requested ' 
            + data['requested'].length + ' books</a>.'
    
    document.getElementById('borrowed-div').innerHTML = asParagraph(count)
    
}

function list(data){
    html = '<hr>' //+ data[0].status.toUpperCase()
    for (i = 0; i < data.length; i++){
        html += asParagraph(data[i]["title"], 'return-search') 
    }
    document.getElementById('list-div').innerHTML = html
}

function signOut(){
    JWT = ""
    html = newButton('loginButton', 'loginForm()', 'LOG IN')
            + " OR "
            + newButton('signUpButton','signUpForm()','SIGN UP')
    document.getElementById('footer').innerHTML = html
    document.getElementById('root').innerHTML = asParagraph(buffer + 'Go in peace!', 'show-account')
    showMessage('You have been safely logged out.')
}

function clickShield(){
    address = 'Salvation Army Church,<br>'
            + 'Prescot Road, Old Swan,<br>'
            + 'Liverpool, L13 3BT.'
    linkedAddress = "<a href='https://oldswanchurch.org.uk' target='_blank'>" + address + "</a>"
    showHide('footer')
    document.getElementById('root').innerHTML = asParagraph(linkedAddress, 'big')
    verse = 'I am unashamed of the gospel,'
            + 'for it is the power of God unto salvation'
            + 'for all who believe. Romans 1:16.'
    john316 = 'For God so loved the world...'
    showMessage(john316)
}

function validate_email(input){
    splitAt = input.split('@')
    if (splitAt.length != 2){return false}
    if (splitAt[0].length == 0){return false}
    if (splitAt[1].split('.') == 0){return false}
    
    return true
}

function clickMessage(){
    hide('message')
}    

function showHide(id) {
    visibility = document.getElementById(id).style.display;
    if (visibility=="none"){show(id)}
    else {hide(id)}
}

function hide(id){
    document.getElementById(id).style.display = "none"
}

function show(id){
    document.getElementById(id).style.display = "block"
}



function invalid_email(){
    sorryMsg = "Sorry, that's not a valid email. Please try again."
    okButton = "<button onClick='hide()'>OK</button>"
    document.getElementById('message').innerHTML = sorryMsg + okButton
    document.getElementById('auth').style.display = "none"
    document.getElementById('message').style.display = "block"
    
}

function showMessage(message){
    okButton = ' [x]'
    document.getElementById('message').innerHTML = message + okButton
    document.getElementById('message').style.display = "block"
}


function postFetch(url, postData, action) {
  fetch(url, {
    method: 'post',
    headers : { 
    'Content-Type': 'application/json',
    'Accept': 'application/json'
   },
    body: JSON.stringify(postData)
  }).then((response) => {
    return response.json();
  }).then((returnedData)  => {
    action(returnedData);
  });
}

function getFetch(url, action){
    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((myJson) => {
        action(myJson);
        }
    );
}

function inputField(type, id, placeholder, className){
    htmlString = "<input type = '" + type
                + "' id = '" + id
                + "' placeholder = '" + placeholder
                + "' class = '" + className
                + "'>"
    return htmlString}

function asParagraph(content, classname) {return "<p class=" + classname + ">"+content+"</p>"}

function asDiv(id, divClass, content) {return "<div id=" + id + " class=" + divClass + ">"+content+"</div>"}

function newButton(id, action, label){return "<button id=" + id + " onClick=" + action + ">" + label + "</button>"}

function zz(){}

# App
*A Single-Page Web App for Bookshop/Lending Library.*

## Stack

Front-End: Vanilla HTML/CSS/JS

Back-End: Python (Flask)

Database: SQLite3

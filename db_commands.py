"""
Includes a variety of commands to make querying the SQLite database simple,
encapsulated in a class called Query.

"""

import sqlite3  #db functionality ~ https://docs.python.org/3/library/sqlite3.html
import string
import os.path
import sys
import csv

class Query():

    def __init__(self, db_filename):
        self.db = db_filename
        self.open()
        self.close()
        
    def open(self):
        """
        Opens connection to database.
        """
        self.conn = sqlite3.connect(self.db)
        self.c = self.conn.cursor()
        
    def close(self):
        """
        Commits changes and closes connection to database.
        """
        self.conn.commit()
        self.conn.close()
                
    def test(self):
        """Shows that tests are successfully importing db_commands"""
        print("connected")
        return "connected"

    def create_table(self, table, columns, or_check=True):
        """
        Creates a table.
        
        Args:
            table (string)
            columns (tuple of strings)
            c: database connection.
        """

        self.open()
        if or_check == True:
            clause = "IF NOT EXISTS "
        print('creating table')
        self.c.execute(f'''CREATE TABLE {clause}{table} {columns}''')
        print('created table')
        self.close()
        
    
    def drop_table(self, table):
        """
        Drops table.
        
        Args:
            table (string)
            c: database connection.
        """
        
        self.open()
        print(f"dropping table '{table}'")
        self.c.execute(f'''DROP TABLE {table}''')
        self.conn.close()
    

    def save_row_to_table(self, table, row, allow_duplicate=False):
        """
        Saves row of data to table.
        
        Args:
            data (list)
        """
        
        self.open()
        print('...saving row to table...')
        
        if allow_duplicate == False:
            # check if duplicate already exists
            if self.search(table, 'key', row[0]):
                print(f'ERROR in trying to save {row} to {table}: row already added!')
                return {'status':'error','error':'key already exists'}
        
        question_marks = ('?,' * len(row))[0:-1] # slice to remove trailing comma
        self.c.executemany(f"INSERT INTO {table} VALUES ({question_marks});", (row,))
        self.close()
        print(f"Row saved to table '{table}' in database '{self.db}': {row}")
        return {'status':'successful'}

    def search(self, table, column, value, with_rowid=False, precise=False):
        """
        Searches column in table for specified value.
        
        Args:
            table (string)
            column (string)
            value
            c: database connection
            with_rowid (Boolean)
        """
        
        self.open()
        print(f'searching for value "{value}" in column "{column}" of table "{table}"')
        if precise==False:
            value = '%'+str(value)+'%'
        
        if with_rowid==True:
            self.c.execute(f"SELECT rowid, * FROM {table} WHERE {column} LIKE ?", (value,))
        else:
            self.c.execute(f"SELECT * FROM {table} WHERE {column} LIKE ?", (value,))
        search_results = self.c.fetchall()
        

        return search_results
        

        
    def full(self, table):
        """
        Shows full table.
        
        Args:
            table (string)
        """
        
        self.open()
        self.c.execute(f"SELECT * FROM {table}")
        full_results = self.c.fetchall()
        print(f"Returning full results for {table} table.")
        return full_results


    def remove_row(self, table, rowid):
        """
        Removes specified row from table.
        
        Args:
            table (string)
            rowid (int)
            c: database connection
        """
        
        self.open()
        print(f'removing row {rowid} from {table}')
        self.c.execute(f"DELETE FROM {table} WHERE rowid=?", (rowid,))
        self.close()

    def remove_duplicate_rows(self, table, repeated_value, all_but=1, column='key'):
        """
        Removes rows with specified duplicate value.
        
        Args:
            table (string)
            repeated_value
            c: database connection
            column (string): default is 'key' which will only remove duplicate rows,
                            but any column could be specified to remove all rows 
                            with duplicate values.
        """
        
        while len(self.search(table, column, repeated_value, with_rowid=True)) > all_but:
            self.remove_row(table, self.search(table, column, repeated_value, with_rowid=True)[all_but][0])
    
    def reboot(self):
        """
        Create necessary tables.
        """
        auth_columns = ('key', 'pwd_hash', 'permission_time')
        
        account_columns = ('key', 'permission', 'phone', 'email',\
                            'postcode', 'first_name', \
                            'last_name', 'date_of_birth')

        admin_columns = ('key','email','role')

        item_columns = ('key', 'owner_id', 'type') 
        
        transaction_columns = ('key', 'account', 'item', 'status',\
                                'init_time', 'fin_time')
        
        messages_columns = ('key', 'from', 'to', 'time', 'message')
        
        books = ('key', 'title', 'format', \
                'first_published', 'this_published', 'pages')
        
        publisher = ('key', 'name')
        
        publisher_item = ('key', 'publisher_key', 'item_key')
        
        creator = ('key', 'last name', 'rest of name')
        
        creator_books = ('key', 'creator_id', 'item_id', 'creator_role', 'creator_ordinal')
        
        value = ('key', 'item_id', 'time', 'pricetag_value', \
                    'replacement_value', 'cost_value')
        
        barcode = ('item_id', 'barcode')
        
        # ~ self.create_table('auth', auth_columns)
        # ~ self.create_table('accounts', account_columns)
        # ~ self.create_table('admins', admin_columns)

        # ~ self.create_table('items', item_columns)
        # ~ self.create_table('transactions', transaction_columns)
        # ~ self.create_table('messages', messages_columns)
        # ~ self.create_table('books', books)
        # ~ self.create_table('publishers', publisher)
        # ~ self.create_table('publisher_item', publisher_item)
        # ~ self.create_table('creator', creator)
        # ~ self.create_table('creator_books', creator_books)
        # ~ self.create_table('value', value)
        # ~ self.create_table('barcode', barcode)
        
    def load(self, table_name):
        """Load table values from CSV.
        
        Args:
            table_name: SQLite table called table_name saved as table_name.csv
        """
        
        directory = str(self.db).replace('.db','')
        with open(os.path.join(sys.path[0], directory, f'{table_name}.csv'), 'r') as csvfile:        
            reader = csv.reader(csvfile, dialect = 'excel')
            col_names = []
            for row in reader:
                if col_names == []:
                    for table in row:
                        col_names.append(table)
                    self.create_table(table_name, tuple(col_names))
                else:
                    self.save_row_to_table(table_name, row, allow_duplicate=True)
        
    def load_all(self):
        """Load all tables from CSV files in /dump folder."""
        
        tables = []
        directory = str(self.db).replace('.db','')
        with open(os.path.join(sys.path[0], directory, 'sqlite_master.t'), 'r') as file:
            for table in file.read().replace('\n','').split(','):
                tables.append(table)
            
        print(tables)
        for table in tables:
            q.load(table)
    
    def dump_all(self):
        """Write all tables to csv files."""
        
        directory = str(self.db).replace('.db','')
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        tables = []
        master = self.full('sqlite_master')
        for element in master:
            tables.append(element[2])
        
        with open(os.path.join(sys.path[0], directory, 'sqlite_master.t'), 'w') as csvfile:
            writer = csv.writer(csvfile, dialect='excel')
            writer.writerow(tables)
            
        for table in tables:
            self.dump(table, directory)
        
    
    def dump(self, table_name, directory):
        """Write tables to csv files."""
        
        if not os.path.exists(directory):
            os.makedirs(directory)
        
        full_table = self.full(table_name)
        
        filename = str(table_name)+".csv"
        col_names = self.col_names(table_name)
        with open(os.path.join(sys.path[0], directory, filename), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, dialect='excel')
            writer.writerow(col_names)
            for row in full_table:
                writer.writerow(row)

    def col_names(self, table_name):
        """Return column names for given table."""
        
        col_names = []
        
        self.open()
        self.c.execute(f"PRAGMA table_info({table_name})")
        data = self.c.fetchall()
        for tuplet in data:
            col_names.append(tuplet[1])
        return col_names
    
    def zz():
        pass
    
if __name__ == '__main__':
    
    q = Query(os.path.join(sys.path[0], 'auth.db'))
    # ~ q.reboot()
    # ~ q.load_all()
    # ~ q.dump_all()
    
    # ~ me = ('007', 'peter@wordandspirit.co', 'admin')
    # ~ sam = ('001', 'sam@wordandspirit.co', 'admin')
    # ~ john = ('006', 'john@wordandspirit.co', 'admin')
    
    # ~ q.save_row_to_table('admins', me)
    # ~ q.save_row_to_table('admins', sam)
    # ~ q.save_row_to_table('admins', john)
    
    print(q.search('admins', 'email', 'sam@wordandspirit.co')[0])
